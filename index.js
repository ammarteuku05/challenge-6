const express = require('express')
const app = express()
const port = 3000
const errorHandler = require('./errorHandler');
const jwt = require('jsonwebtoken')
const { body, validationResult } = require('express-validator');
const UserGamesController = require('./controller/controller')
require('dotenv').config()

app.use(express.urlencoded({extended: false}))
app.use(express.json())
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const multer = require('multer')
const storage = require('./services/multer_storage')

const morgan = require('morgan')

const upload = multer(
  {
    storage: storage,
    fileFilter: (req, file, cb) => {
      if (file.mimetype === 'video/mp4' || file.mimetype === 'video/mkv') {
        cb(null, true)
      } else {
        cb(new Error('File should be an video'), false)
      }
    }
  }
)

app.get('/lists',
  (req, res, next) => {
    if (req.headers.authorization) {
      const decodedToken = jwt.decode(req.headers.authorization)
      req.user = decodedToken;
      next();
    } else {
      throw {
        status: 401,
        message: 'Unauthorized'
      }
    }
}, UserGamesController.list)
app.get('/user/:id', 
UserGamesController.getById)
app.post('/register',[
  body('email')
    .notEmpty()
    .withMessage('email should not be empty')
    ,
  body('password')
    .notEmpty(),
  body('fullname')
    .notEmpty(),
],
(req, res, next) => {
const errors = validationResult(req);
if (!errors.isEmpty()) {
  next({
    status: 400,
    message: errors.array()
  })
} else {
  next()
}
}, UserGamesController.createusergames)
app.post('/login', UserGamesController.loginUserGames)
app.post('/login-google', UserGamesController.loginGoogle)
app.post('/login-facebook', UserGamesController.loginFacebook)
app.post('/forgot-password', UserGamesController.sendForgotPasswordToken)
app.post('/usergamehistories', 
upload.single('video'),
(req, res, next) => {
  if (req.headers.authorization) {
    const decodedToken = jwt.decode(req.headers.authorization)
    req.user = decodedToken;
    next();
  } else {
    throw {
      status: 401,
      message: 'Unauthorized'
    }
  }
},
UserGamesController.createusergameshistories)

app.post('/userbiodata',(req, res, next) => {
  if (req.headers.authorization) {
    const decodedToken = jwt.decode(req.headers.authorization)
    req.user = decodedToken;
    next();
  } else {
    throw {
      status: 401,
      message: 'Unauthorized'
    }
  }
}, 
UserGamesController.createusergamebiodata)
app.put('/user/:id', (req, res, next) => {
  if (req.headers.authorization) {
    const decodedToken = jwt.decode(req.headers.authorization)
    req.user = decodedToken;
    next();
  } else {
    throw {
      status: 401,
      message: 'Unauthorized'
    }
  }
},
UserGamesController.update)

app.delete('/user/:id', (req, res, next) => {
  if (req.headers.authorization) {
    const decodedToken = jwt.decode(req.headers.authorization)
    req.user = decodedToken;
    next();
  } else {
    throw {
      status: 401,
      message: 'Unauthorized'
    }
  }
},
UserGamesController.delete)

app.use(errorHandler)
app.use(morgan('dev'))

app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})