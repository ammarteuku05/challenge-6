'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameHistories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGameHistories.belongsTo(models.UserGames, { foreignKey: 'user_game_id' })
    }
  }
  UserGameHistories.init({
    user_game_id: DataTypes.INTEGER,
    durasi: DataTypes.STRING,
    jam: DataTypes.STRING,
    skor: DataTypes.INTEGER,
    video: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserGameHistories',
  });
  return UserGameHistories;
};