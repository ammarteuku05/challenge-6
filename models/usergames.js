'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcryptjs')

module.exports = (sequelize, DataTypes) => {
  class UserGames extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGames.hasOne(models.UserBiodatas, { foreignKey: 'user_game_id', as: 'Biodata' })
      UserGames.hasMany(models.UserGameHistories, { foreignKey: 'user_game_id', as: 'Histories' })
    }
  }
  UserGames.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    fullname: DataTypes.STRING,
    forgot_pass_token: DataTypes.STRING,
    forgot_pass_token_expired_at: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'UserGames',
  });
  
  UserGames.addHook('beforeCreate', (user, options) => {

    // enkripsi / hash si password
    const salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(user.password, salt);
    // user.password = hash;
  });
  
  UserGames.addHook('beforeUpdate', (user) => {
    if (user.password) {
       // enkripsi / hash si password
    const salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(user.password, salt);
    // user.password = hash;
    }
  })
  return UserGames;
};
