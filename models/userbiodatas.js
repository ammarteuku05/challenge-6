'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserBiodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserBiodatas.belongsTo(models.UserGames, { foreignKey: 'user_game_id' })
    }
  }
  UserBiodatas.init({
    user_game_id: DataTypes.INTEGER,
    pekerjaan: DataTypes.STRING,
    umur: DataTypes.INTEGER,
    ttl: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserBiodatas',
  });
  return UserBiodatas;
};