const nodemailer = require('nodemailer')

module.exports = async (from, to, subject, text, html) => {
  let transporter = nodemailer.createTransport({
    host: "smtp-relay.sendinblue.com",
    port: 587,
    secure: false,
    auth: {
      user: "",
      pass: ""
    }
  });

  let info = await transporter.sendMail({
    from: from, // sender address
    to: to, // list of receivers
    subject: subject,
    text: text,
    html: html
  });

  return info
}