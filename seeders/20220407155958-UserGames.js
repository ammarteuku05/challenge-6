'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('UserGames', [
     {
      email: "ammar@mail.com",
      password: "teuku",
      fullname: "teuku ammar",
      createdAt: new Date(),
      updatedAt: new Date()
     }
   ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('UserGames', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
