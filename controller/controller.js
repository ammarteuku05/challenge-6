const { UserGames, UserGameHistories, UserBiodatas} = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
const {OAuth2Client} = require('google-auth-library')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)
const axios = require('axios')
const sendEmail = require('../services/mailer');
class UserGamesController {
  static list(req, res) {
   UserGames.findAll({
       attributes: ['id', 'username'], 
    })
    .then((data) => {
      res.status(200).json(data)
    })
    .catch((error) => {
      res.status(500).json(error)
    })
  }

  static getById(req, res) {
    UserGames.findAll({
      where: {
        id: req.params.id
      },
      attributes: {
        exclude: ['createdAt', 'updatedAt']
    },
    })
      .then((data) => {
        res.status(200).json(data)
      })
      .catch((err) => {
        res.status(500).json(err)
      })
  }

  static async createusergames(req, res, next) {

    try {
      const user = await UserGames.findOne({
        where: {
          email: req.body.email
        }
      })

      if (user) {
        throw {
          status: 400,
          message: 'Email already used'
        }
      }
      
      const userRegister = await UserGames.create({
       fullname:req.body.fullname,
       email:req.body.email,
       password:req.body.password,
      })

      if (userRegister){
        await sendEmail('ammarteuku09@gmail.com', req.body.email, 'Your Account Has Registered', `<body> Hai ${req.body.name} your account has been created at ${new Date} </body>`)
        res.status(201).json({
          message: 'Successfully create user'
        })
      }

      
      res.status(200).json({
        message: 'Successfully create usergame'
      })
    } catch (error) {
      next(err)
    }
  }

  static async loginUserGames (req, res, next) {
    try {
      const user = await UserGames.findOne({
        where: {
         email: req.body.email
        }
      })
      if (!user) {
        throw {
          status: 401,
          message: 'Invalid email or password'
        }
      } else {
        if (bcrypt.compareSync(req.body.password, user.password)) {
          // mengeluarkan token
          let token = jwt.sign({ id: user.id,  email: user.email, fullname: user.fullname, }, process.env.JWT_SECRET);
          res.status(200).json({
            message: 'Login success',
            token,
          })
        } else {
          throw {
            status: 401,
            message: 'Invalid email or password'
          }
        }
      }
     } catch (error) {
      next(error);
     }
    }

    static async loginGoogle(req, res, next) {
      try {
        const token = await client.verifyIdToken({
          idToken: req.body.id_token,
          audience: process.env.GOOGLE_CLIENT_IDs
        })
        const payload = token.getPayload()
        const user = await User.findOne({
          where: {
            email: payload.email
          }
        })
        if (user) {
          const jwtToken = jwt.sign({
            id: user.id,
            email: user.email
          }, process.env.JWT_SECRET)
          res.status(200).json({
            token: jwtToken
          })
        } else {
          const registeredUser = await User.create({
            email: payload.email
          })
          const jwtToken = jwt.sign({
            id: registeredUser.id,
            email: registeredUser.email
          }, process.env.JWT_SECRET)
          res.status(200).json({
            token: jwtToken
          })
        }
      } catch(err) {
        next(err)
      }
    }

    static async loginFacebook(req, res, next) {
      try {
        const response = await axios.get(`https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${req.body.token}`)
        const user = await User.findOne({
          where: {
            email: response.data.email
          }
        })
        if (user) {
          const jwtToken = jwt.sign({
            id: user.id,
            email: user.email
          }, process.env.JWT_SECRET)
          res.status(200).json({
            token: jwtToken
          })
        } else {
          const registeredUser = await User.create({
            email: response.data.email
          })
          const jwtToken = jwt.sign({
            id: registeredUser.id,
            email: registeredUser.email
          }, process.env.JWT_SECRET)
          res.status(200).json({
            token: jwtToken
          })
        }
      } catch(err) {
        next(err)
      }
    }

  static async createusergameshistories(req, res) {

    try {
      if (req.file) {
        req.body.video = `http://localhost:3000/${req.file.filename}`
      }

      const user = await UserGameHistories.create({
        user_game_id:req.user.id,
        durasi:req.body.durasi,
        jam:req.body.jam,
        skor:req.body.skor,
        video: req.body.video
      })
      res.status(201).json({
        data:{
          user
        }
      })
    } catch (err) {
      res.status(500).json(err)
    }
  }

  static createusergamebiodata(req, res) {
    UserBiodatas.create({
      user_game_id:req.body.user_game_id,
      pekerjaan:req.body.pekerjaan,
      ttl:req.body.ttl,
      umur:req.body.umur,
    })
      .then((data) => {
        console.log(data)
        res.status(200).json(data)
      })
      .catch((err) => {
        res.status(500).json(err)
      })
  }

  static update(req, res) {
    UserGames.update({
      email:req.body.email,
      password:req.body.password,
    }, {
      where: {
        id: req.params.id
      },
      returning: true,
    })
    .then((data) => {
      res.status(200).json(data[1][0])
    })
    .catch((error) => {
      res.status(500).json(error)
    }) 
  }

  static delete(req, res) {
    UserGames.destroy({
      where: {
        id: req.params.id
      }
    })
    .then((data) => {
      res.status(200).json({
        message: 'Succesfully delete data'
      })
    })
    .catch((error) => {
      res.status(500).json(error)
    })
  }

  static async sendForgotPasswordToken(req, res, next) {
    try {
      const user = await UserGames.findOne({
        where: {
          email: req.body.email
        }
      })
  
      if (!user) {
        throw {
          status: 400,
          message: 'Invalid email'
        }
      } else {
        const otp = otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false });
        const salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(otp, salt)
        await Users.update({
          forgot_pass_token: hash,
          forgot_pass_token_expired_at: new Date(new Date().getTime() + 5 * 60000)
        }, {
          where: {
            email: req.body.email
          }
        })
        const html = `
        <pre>
        Token Anda: ${otp} <br>
        Email terbuat otomatis pada ${new Date()}
        <pre>
        `
        await sendEmail('ammarteuku09@gmail.com', req.body.email, html, null, 'Your Forgot Password Token')
        res.status(200).json({
          message: 'Succesfully send email'
        })
        
      }
    } catch(err) {
      next(err)
    }
  }
}

module.exports = UserGamesController 